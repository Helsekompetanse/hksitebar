<?php

/**
 * Implementation of theme_hook().
 */
function hksitebar_theme($existing, $type, $theme, $path) {
  return array(
    'hksitebar' => array(
      'variables' => array(
        'variables' => NULL,
      ),
      'template' => 'hksitebar',
    ),
  );
}

function hksitebar_menu() {
  $items = array();

  $items['admin/config/sitebar'] = array(
    'page callback' => 'hksitebar_admin_config',
    'type' => MENU_NORMAL_ITEM,
    'menu_name' => 'Helsekompetanse',
  );

  return $items;
}

/**
 * Implementation of template_preprocess_page().
 */
function hksitebar_preprocess_page(&$vars) {
  global $user;
  global $base_path;

  // Path to this module
  $module_path = drupal_get_path('module', 'hksitebar');

  $javascript_settings = array('hksitebar' => array());

  $javascript_settings['hksitebar']['user']['authenticated'] = $user->uid > 0 ? TRUE : FALSE;

  drupal_add_js($javascript_settings, 'setting');

  // We need modernizr - we can then do media query checks in javascript.
  $modernizr = libraries_get_path('modernizr') . '/modernizr.min.js';
  drupal_add_js($modernizr);

  // Add our own javascript
  drupal_add_js($module_path . '/js/hksitebar.js');

  // Add module CSS.
  drupal_add_css($module_path . '/css/hksitebar.css');

  // This array is passed to template
  $variables = array();

  // Add some custom variables is user is logged in
  if ($user->uid > 0) {

    $variables['account_url'] = url('user/' . $user->uid);
    $variables['realname'] = $user->name;

    if (module_exists('realname')) {
      $variables['realname'] = format_username($user);
    }

    $variables['user_picture'] = theme('user_picture', array('account' => $user, 'image_style' => 'sitebar_user_picture_icon', 'no-link' => TRUE));

    $variables['user_menu'] = _hksitebar_get_user_menu();
  }
  else {

    $q = $_GET['q'];

    $anon_link = '';
    $anon_link_options = array(
      'attributes' => array(
        'class' => 'btn btn-primary btn-small',
      ),
    );

    if ($q == 'user' || $q == 'user/login') {
      $anon_link = '<span>' . t('Do not have an account?') . '</span>' . l(t('Create new account'), 'user/register', $anon_link_options);
    }
    else {

      // If we have a destination in url, attach to login url
      if (array_key_exists('destination', $_GET) && !empty($_GET['destination'])) {
        $query = array(
          'destination' => $_GET['destination'],
        );

        $anon_link_options['query'] = array(
          'destination' => $_GET['destination'],
        );
      }
      $anon_link = l(t('Login'), 'user/login', $anon_link_options);
    }

    $variables['not_logged_in_link'] = '<div class="not-logged-in">' . $anon_link . '</div>';
  }

  // Send user object to template. User is NULL if anonymous
  $variables['account'] = ($user->uid > 0 ? $user : NULL);

  // A link to current sites home page
  $variables['front_page'] = variable_get('legacy_host', 'http://helsekompetanse.no');

  // Site logo
  $variables['logo'] = $base_path . $module_path . '/images/logo.png';

  // Theme and attach sitebar to page
  $vars['hksitebar'] = theme('hksitebar', $variables);
}

function _hksitebar_get_user_menu() {
  global $user;

  $links = array(
    array(
      'title' => t('My profile'),
      'href' => 'user/' . $user->uid,
    ),
    array(
      'title' => t('Edit profile'),
      'href' => 'user/' . $user->uid . '/edit',
    ),
    array(
      'title' => t('Logout'),
      'href' => 'user/logout',
    ),
  );

  $links = theme('links', array('links' => $links));

  // dpm($links);

  return $links;

}
