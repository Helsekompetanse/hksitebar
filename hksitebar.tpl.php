
<header id="hksitebar">
  <section id="main-container">
    <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="hksitebar_logo" id="logo">
      <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" class="hksitebar_logo-image" />
    </a>

    <section id="hksitebar-user">
      <?php if (user_is_logged_in()) : ?>
      <a href="<?php print $account_url; ?>" class="logged-in">
        <?php print theme('user_picture', array('account' => $account, 'image_style' => 'sitebar_user_picture_icon', 'no-link' => TRUE)); ?>
        <span class="username-wrapper"><?php print $realname; ?></span>
      </a>
      <?php else : ?>
      <?php print ($not_logged_in_link ? $not_logged_in_link : ''); ?>
      <?php endif; ?>
    </section>
  </section>

  <?php if (!empty($user_menu)) : ?>
  <section class="sub-containers">
    <section id="user-subsection" class="sub-container">
      <?php print $user_menu; ?>
    </section>
  </section>
  <?php endif; ?>


</header>
