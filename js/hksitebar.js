/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {


// To understand behaviors, see https://drupal.org/node/756722#behaviors
Drupal.behaviors.hksitebar = {
  attach: function(context, settings) {

    $('#hksitebar a.logged-in').click(function(){

      // Get a handle to the user menu
      var userMenu = $('#hksitebar .sub-containers #user-subsection');
      userMenu.toggle();

      return false;
    });

    $(document).click(function(){
      var sub = $('#hksitebar .sub-containers .sub-container');
      if (sub.is(':visible'))
        sub.hide();
    });
  }
};


})(jQuery, Drupal, this, this.document);
